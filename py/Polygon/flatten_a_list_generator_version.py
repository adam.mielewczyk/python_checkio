#!/usr/bin/env checkio --domain=py run flatten-a-list-generator-version

# 
# END_DESC


def flat_list(array):
    if type(array) is list:
        for element in array:
            for ans in flat_list(element):
                yield ans
    else:
        yield array

if __name__ == '__main__':
    print(list(flat_list([1, [2, 2, 2], 4])))
    assert list(flat_list([1, 2, 3])) == [1, 2, 3], "First"
    assert list(flat_list([1, [2, 2, 2], 4])) == [1, 2, 2, 2, 4], "Second"
    assert list(flat_list([[[2]], [4, [5, 6, [6], 6, 6, 6], 7]])) == [2, 4, 5, 6, 6, 6, 6, 6, 7], "Third"
    assert list(flat_list([-1, [1, [-2], 1], -1])) == [-1, 1, -2, 1, -1], "Four"
    print('Done! Check it')