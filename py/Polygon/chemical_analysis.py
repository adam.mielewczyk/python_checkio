#!/usr/bin/env checkio --domain=py run chemical-analysis

# As the input you will get the chemical formula and the number N. Your task is to create a list of the chemical elements, which are in the formula in the amount of >= N.
# Pay attention, that in the some formulas will be used brackets - () and [].This articlewill help you to open the brackets and don't make mistake while counting.
# 
# Input:Chemical formula and the number of atoms.
# 
# Output:List of the chemical elements, sorted in the alphabetical order.
# 
# Precondition:
# 1<= different elements<= 10
# 
# 
# END_DESC
import collections


def find_first_int(string: str):
    integer = ""
    try:
        if string[0].isdigit():
            for char in string:
                if char.isdigit():
                    integer += char
                else:
                    return int(integer)
        else:
            return 1
    except IndexError:
        return 1
    return int(integer)

def generate_formula_dict(string: str):
    dict = {}
    for i, char in enumerate(string):
        if char.isupper():
            name = char
            try:
                if string[i+1].islower():
                    name += string[i+1]
            except IndexError:
                pass
            try:
                dict[name] = dict[name] + find_first_int(string[i+len(name):])
            except KeyError:
                dict[name] = find_first_int(string[i+len(name):])
    return dict

def atoms(formula, limit):
    # różne oznaczenia nawiasów tylko przeszkadzają
    formula = formula.replace(")","]")
    formula = formula.replace("(","[")
    # sprawdzam ile jest nawiasów i je rozkładam w pętli
    nesting_level = formula.count("]")
    for i in range(nesting_level):
        deepest_brackets = [None, formula.find("]") + 1]
        deepest_brackets[0] = formula.rfind("[", 0, deepest_brackets[1])
        multiply_by = find_first_int(formula[deepest_brackets[1]:])
        multiplied_brackets = ""
        for key, val in generate_formula_dict(formula[deepest_brackets[0]+1:deepest_brackets[1]-1]).items():
            multiplied_brackets += key + str(val*multiply_by)
        formula = formula[:deepest_brackets[1]] + formula[deepest_brackets[1] + len(str(multiply_by)):]
        formula = formula.replace(formula[deepest_brackets[0]:deepest_brackets[1]], multiplied_brackets, 1)
    ans = []
    od = collections.OrderedDict(sorted(generate_formula_dict(formula).items()))
    for key, val in od.items():
        if val >= limit:
            ans.append(key)
    return ans

if __name__ == '__main__':
    print("Example:")
    print(atoms('Mg3(PO4)2', 2))

    #These "asserts" using only for self-checking and not necessary for auto-testing
    assert atoms('C2H5OH', 2) == ['C', 'H']
    assert atoms('H2O', 2) == ['H']
    assert atoms('Mg(OH)2', 1) == ['H', 'Mg', 'O']
    assert atoms('K4[ON(SO3)2]2', 4) == ['K', 'O', 'S']
    print("Coding complete? Click 'Check' to earn cool rewards!")