#!/usr/bin/env checkio --domain=py run army-battles

# 
# END_DESC

# Taken from mission The Warriors

class Warrior:
    def __init__(self):
        self.health = 50
        self.attack = 5

    @property
    def is_alive(self):
        return True if self.health > 0 else False


class Knight(Warrior):
    def __init__(self):
        super().__init__()
        self.attack = 7

def fight(unit_1, unit_2):
    unit_2.health -= unit_1.attack
    if unit_2.is_alive:
        fight(unit_2, unit_1)
    if unit_1.is_alive:
        return True
    else:
        return False

class Army:
    def __init__(self):
        self.units = []
    def add_units(self, unit_type, quantity):
        for i in range(quantity):
            self.units.append(unit_type())
    def get_first_alive(self):
        for unit in self.units:
            if unit.is_alive:
                return unit
        return None

class Battle:
    def fight(self, army1, army2):
        while True:
            unit_from_army1 = army1.get_first_alive()
            unit_from_army2 = army2.get_first_alive()
            if unit_from_army1 == None:
                return False
            elif unit_from_army2 == None:
                return True
            fight(unit_from_army1, unit_from_army2)


if __name__ == '__main__':
    #These "asserts" using only for self-checking and not necessary for auto-testing

    chuck = Warrior()
    bruce = Warrior()
    carl = Knight()
    dave = Warrior()
    mark = Warrior()

    assert fight(chuck, bruce) == True
    assert fight(dave, carl) == False
    assert bruce.is_alive == False
    assert carl.is_alive == True
    assert dave.is_alive == False
    assert fight(carl, mark) == False
    assert carl.is_alive == False

    print("Coding complete? Let's try tests!")

if __name__ == '__main__':
    #These "asserts" using only for self-checking and not necessary for auto-testing
    
    #fight tests
    chuck = Warrior()
    bruce = Warrior()
    carl = Knight()
    dave = Warrior()
    mark = Warrior()

    assert fight(chuck, bruce) == True
    assert fight(dave, carl) == False
    assert chuck.is_alive == True
    assert bruce.is_alive == False
    assert carl.is_alive == True
    assert dave.is_alive == False
    assert fight(carl, mark) == False
    assert carl.is_alive == False

    #battle tests
    my_army = Army()
    my_army.add_units(Knight, 3)
    
    enemy_army = Army()
    enemy_army.add_units(Warrior, 3)

    army_3 = Army()
    army_3.add_units(Warrior, 20)
    army_3.add_units(Knight, 5)
    
    army_4 = Army()
    army_4.add_units(Warrior, 30)

    battle = Battle()

    assert battle.fight(my_army, enemy_army) == True
    assert battle.fight(army_3, army_4) == False
    print("Coding complete? Let's try tests!")