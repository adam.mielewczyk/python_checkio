#!/usr/bin/env checkio --domain=py run house-password

# 
# END_DESC
import string


def checkio(data: str) -> bool:
    return True if len(data) >= 10 \
                   and "".join(filter(str.islower, data)) \
                   and "".join(filter(str.isupper, data)) \
                   and "".join(filter(str.isdigit, data)) else False

#Some hints
#Just check all conditions


if __name__ == '__main__':
    #These "asserts" using only for self-checking and not necessary for auto-testing
    assert checkio('ULFFunH8ni') == True, "1st example"
    assert checkio('bAse730onE4') == True, "2nd example"
    assert checkio('asasasasasasasaas') == False, "3rd example"
    assert checkio('QWERTYqwerty') == False, "4th example"
    assert checkio('123456123456') == False, "5th example"
    assert checkio('QwErTy911poqqqq') == True, "6th example"
    print("Coding complete? Click 'Check' to review your tests and earn cool rewards!")