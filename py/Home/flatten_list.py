#!/usr/bin/env checkio --domain=py run flatten-list

# 
# END_DESC

def flat(array, ans):
    for element in array:
        if type(element) == list:
            flat(element, ans)
        else:
            ans.append(element)

def flat_list(array):
    ans = []
    flat(array, ans)
    return ans

if __name__ == '__main__':
    print(flat_list([1, [2, 2, 2], 4]))
    assert flat_list([1, 2, 3]) == [1, 2, 3], "First"
    assert flat_list([1, [2, 2, 2], 4]) == [1, 2, 2, 2, 4], "Second"
    assert flat_list([[[2]], [4, [5, 6, [6], 6, 6, 6], 7]]) == [2, 4, 5, 6, 6, 6, 6, 6, 7], "Third"
    assert flat_list([-1, [1, [-2], 1], -1]) == [-1, 1, -2, 1, -1], "Four"
    print('Done! Check it')