#!/usr/bin/env checkio --domain=py run pawn-brotherhood

# 
# END_DESC

def safe_pawns(pawns: set) -> int:
    protected = []
    for key in pawns:
        attack_posi = [str(chr(ord(key[0])-1)) + str(int(key[1])+1), str(chr(ord(key[0])+1)) + str(int(key[1])+1)]
        for pawn in pawns:
            for i in range(2):
                if attack_posi[i] == pawn and not pawn in protected:
                    protected.append(pawn)
    return len(protected)

if __name__ == '__main__':
    #These "asserts" using only for self-checking and not necessary for auto-testing
    assert safe_pawns({"b4", "d4", "f4", "c3", "e3", "g5", "d2"}) == 6
    assert safe_pawns({"b4", "c4", "d4", "e4", "f4", "g4", "e5"}) == 1
    print("Coding complete? Click 'Check' to review your tests and earn cool rewards!")