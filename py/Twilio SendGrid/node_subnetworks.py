#!/usr/bin/env checkio --domain=py run node-subnetworks

# Sometimes damaged nodes are unrecoverable. In that case, people that were connected to the crushed node must migrate to another district while administration attempts to fix the node.
# 
# But if a crushed node disconnects multiple districts from one another, then the network splits into two sub-networks and every sub-network should have their own Mayor. And Mayors must use pigeons for mailing between each other. In that case, when the network is split you don’t need hundreds of pigeons.
# 
# Your mission is to figure out how many Mayors you need to control the entire city when some nodes are crushed. In other words, you need to figure out how many sub-networks will be formed after some nodes are crush and not recovered.
# 
# 
# 
# 
# END_DESC

def find_next_to_check(net):
    for edge in net:
        for i in range(2):
            if edge[i] != None:
                return edge[i]
    return None

def subnetworks(net, crushes):
    #usuwam z sieci zniszczone elementy
    for crush in crushes:
        for edge in net:
            for i in range(2):
                if crush == edge[i]:
                    edge[i] = None
    subnetworks_counter = 0
    to_check = find_next_to_check(net)
    while find_next_to_check(net) is not None:
        subnetworks_counter += 1
        to_check = [find_next_to_check(net)]
        #iteruje po każdym obiekcie jaki powinienem sprawdzić w tej podsieci
        for element in to_check:
            #iteruje po wszystkich obiektach w sieci
            for edge in net:
                for i in range(2):
                    if edge[i] == element:
                        #"usuwam" szukany obiekt
                        edge[i] = None
                        inverted_i = 0 if i == 1 else 1
                        if edge[inverted_i] is not None:
                            #jeśli był połączony z innym obiektem dodaje go do poszikiwań w tej podsieci
                            to_check.append(edge[inverted_i])
                            edge[inverted_i] = None
        #gdy skończyłem przeszukiwać całą podsieć zaczynam poszukiwania od pierwszego znalezionego elementu w sieci
        to_check = find_next_to_check(net)
    return subnetworks_counter

if __name__ == '__main__':
    #These "asserts" using only for self-checking and not necessary for auto-testing
    assert subnetworks([
            ['A', 'B'],
            ['B', 'C'],
            ['C', 'D']
        ], ['B']) == 2, "First"
    assert subnetworks([
            ['A', 'B'],
            ['A', 'C'],
            ['A', 'D'],
            ['D', 'F']
        ], ['A']) == 3, "Second"
    assert subnetworks([
            ['A', 'B'],
            ['B', 'C'],
            ['C', 'D']
        ], ['C', 'D']) == 1, "Third"
    print('Done! Check button is waiting for you!')