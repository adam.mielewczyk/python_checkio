#!/usr/bin/env checkio --domain=py run node-crucial

# 
# END_DESC
import copy


def find_next_to_check(net):
    for node in net:
        for i in range(2):
            if node[i] != None:
                return node[i]
    return None

def most_crucial(net, users):
    # tworze liste z wszystkimi elementami
    nodes = []
    lowest_happiness = sum(users.values())**2 + 1
    significant_nodes = []
    for edge in net:
        for i in range(2):
            if edge[i] not in nodes:
                nodes.append(edge[i])
    for crushed_node in nodes:
        happiness_counter = users[crushed_node]
        temp_net = copy.deepcopy(net)
        # usuwam z sieci zniszczony element
        for edge in temp_net:
            for i in range(2):
                if crushed_node == edge[i]:
                    edge[i] = None
        to_check = find_next_to_check(temp_net)
        while find_next_to_check(temp_net) is not None:
            subnetwork_happiness = 0
            to_check = [find_next_to_check(temp_net)]
            # iteruje po każdym obiekcie jaki powinienem sprawdzić w tej podsieci
            for element in to_check:
                subnetwork_happiness += users[element]
                # iteruje po wszystkich obiektach w sieci
                for edge in temp_net:
                    for i in range(2):
                        if edge[i] == element:
                            # "usuwam" szukany obiekt
                            edge[i] = None
                            inverted_i = 0 if i == 1 else 1
                            if edge[inverted_i] is not None:
                                # jeśli był połączony z innym obiektem dodaje go do poszikiwań w tej podsieci
                                to_check.append(edge[inverted_i])
                                edge[inverted_i] = None
            # gdy skończyłem przeszukiwać całą podsieć zaczynam poszukiwania od pierwszego znalezionego elementu w sieci
            happiness_counter += subnetwork_happiness**2
            to_check = find_next_to_check(temp_net)
        if lowest_happiness > happiness_counter:
            significant_nodes = [crushed_node]
            lowest_happiness = happiness_counter
        elif lowest_happiness == happiness_counter:
            significant_nodes.append(crushed_node)
    return significant_nodes

if __name__ == '__main__':
    #These "asserts" using only for self-checking and not necessary for auto-testing
    assert most_crucial([
            ['A', 'B'],
            ['B', 'C']
        ],{
            'A': 10,
            'B': 10,
            'C': 10
        }) == ['B'], 'First'

    assert most_crucial([
            ['A', 'B']
        ],{
            'A': 20,
            'B': 10
        }) == ['A'], 'Second'

    assert most_crucial([
            ['A', 'B'],
            ['A', 'C'],
            ['A', 'D'],
            ['A', 'E']
        ],{
            'A': 0,
            'B': 10,
            'C': 10,
            'D': 10,
            'E': 10
        }) == ['A'], 'Third'

    assert most_crucial([
            ['A', 'B'],
            ['B', 'C'],
            ['C', 'D']
        ],{
            'A': 10,
            'B': 20,
            'C': 10,
            'D': 20
        }) == ['B'], 'Forth'

    print('Nobody expected that, but you did it! It is time to share it!')