#!/usr/bin/env checkio --domain=py run stressful-subject

# 
# END_DESC

import re
import string


def is_stressful(subj):
    """
        recognize stressful subject
    """
    if subj.isupper() or subj.endswith("!!!"):
        return True
    red_words = ["help", "asap", "urgent"]
    subj_only_letters = ""
    for char in subj:
        if char.isalpha():
            subj_only_letters += char.lower()
    subj_only_letters_without_repeat = ""
    for i, letter in enumerate(subj_only_letters):
        try:
            if letter != subj_only_letters[i+1]:
                subj_only_letters_without_repeat += letter
        except IndexError:
            subj_only_letters_without_repeat += subj_only_letters[-1:]
    for word in red_words:
        if subj_only_letters_without_repeat.find(word) != -1:
            return True
    return False

if __name__ == '__main__':
    #These "asserts" are only for self-checking and not necessarily for auto-testing
    assert is_stressful("h!e!l!") == False, "First"
    assert is_stressful("I neeed HELP") == True, "Second"
    print('Done! Go Check it!')