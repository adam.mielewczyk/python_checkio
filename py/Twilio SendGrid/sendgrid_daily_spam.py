#!/usr/bin/env checkio --domain=py run sendgrid-daily-spam

# To solve this mission you must use theSendGrid API Key. When you click "Run" you will see the results of using your API key with your data, but if you click "Check" your solution will be tested using our data.
# 
# You are massively sending thousands and thousands emails every day, meanwhile experimenting with subject lines and the message itself.SendGridallows you to see statistics of your spam reports.
# 
# Your mission is to figure out how many spam reports you receive on a specific day.
# 
# Input:Day as a string in format 'YYYY-MM-DD'
# 
# Output:Int. The amount of spam reports
# 
# 
# END_DESC
import json
from datetime import datetime

import sendgrid

API_KEY = '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'

sg = sendgrid.SendGridAPIClient(API_KEY)

def how_spammed(str_date):
    sg = sendgrid.SendGridAPIClient(API_KEY)
    str_date = str_date.split("-")
    timestamp_start = int(datetime(int(str_date[0]), int(str_date[1]), int(str_date[2])).timestamp())
    timestamp_end = int(datetime(int(str_date[0]), int(str_date[1]), int(str_date[2]),23,59,59).timestamp())

    params = {
        'start_time': timestamp_start,
        'end_time': timestamp_end
    }

    response = sg.client.suppression.spam_reports.get(query_params=params)
    data = json.loads(response.body)
    return len(data)

if __name__ == '__main__':
    #These "asserts" using only for self-checking and not necessary for auto-testing
    print('You had {} spam reports in 2016-01-01'.format(how_spammed('2016-01-01')))
    print('Check your results')