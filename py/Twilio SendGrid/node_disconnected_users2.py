#!/usr/bin/env checkio --domain=py run node-disconnected-users2

# 
# END_DESC

def connected_users(net, users, source):
    connected = [source]
    to_check = [source]
    for node_to_check in to_check:
        for edge in net:
            for i in range(2):
                if node_to_check == edge[i] and ( not edge[1 if i == 0 else 0] in connected):
                    connected.append(edge[1 if i == 0 else 0])
                    to_check.append(edge[1 if i == 0 else 0])

    return to_check



def disconnected_users(net, users, source, crushes):
    number_of_users = sum([users[element] for element in connected_users(net, users, source)])
    for crush in crushes:
        for node in net:
            for i in range(2):
                if crush == node[i]:
                    node[0] = None
                    node[1] = None
    connected_number = sum([users[element] for element in connected_users(net, users, source)])
    if source in crushes:
        return number_of_users
    return number_of_users - connected_number

if __name__ == '__main__':
    #These "asserts" using only for self-checking and not necessary for auto-testing
    assert disconnected_users([
        ['A', 'B'],
        ['B', 'C'],
        ['C', 'D']
    ], {
        'A': 10,
        'B': 20,
        'C': 30,
        'D': 40
    },
        'A', ['C']) == 70, "First"

    assert disconnected_users([
        ['A', 'B'],
        ['B', 'D'],
        ['A', 'C'],
        ['C', 'D']
    ], {
        'A': 10,
        'B': 0,
        'C': 0,
        'D': 40
    },
        'A', ['B']) == 0, "Second"

    assert disconnected_users([
        ['A', 'B'],
        ['A', 'C'],
        ['A', 'D'],
        ['A', 'E'],
        ['A', 'F']
    ], {
        'A': 10,
        'B': 10,
        'C': 10,
        'D': 10,
        'E': 10,
        'F': 10
    },
        'C', ['A']) == 50, "Third"

    print('Done. Try to check now. There are a lot of other tests')